/**
 * author: davyx8
 * date:  29.12.15
 */
#include "Lyrical.h"

/**
 * default constructor
 */
Lyrical::Lyrical() : Song()
{
}

/**
   *simple constructor
   *
   */
Lyrical::Lyrical(string name, map<string, int> &tags, string author,
                 map<string, int> &lyrics) :
                        Song(name, tags,  "lyrics by: " + author),
                        lyrics(lyrics), author(author)
{
}


/**
 *
 * return the songs score
 */
int Lyrical::getScore(const string& query)
{
    int tagScore = tags[query] * Song::tagW;
    int lyricScore = lyrics[query] * Song::LyricsW;
    return tagScore + lyricScore;
}
