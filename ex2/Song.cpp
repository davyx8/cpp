/**
* author: davyx8
* date:  29.12.15
*/

#include "Song.h"

#define PRINT_SEP "\t"

extern int Song::tagW;
extern int Song::instrumentW;
extern int Song::bpmW;
extern int Song::LyricsW;

/**
 * default constructor
 */
Song::Song()
{
};

/**
 * default simple constructor
 */
Song::Song(string &title, map<string, int> &tags, string data) :
    title(title),
    tags(tags),
    Data(data)
{ }

/**
 * default destructor
 */
Song::~Song()
{
}

/**
 * set tagw
 */
void Song::setTagW(int Weight)
{
    Song::tagW = Weight;
}

/**
 * sets instrument Weight
 */
void Song::setInstrumentsW(int Weight)
{
    Song::instrumentW = Weight;
}

/**
 * sets bpm Weight
 */
void Song::setBpmW(int Weight)
{
    Song::bpmW = Weight;
}

/**
 * sets lyrics Weight
 */
void Song::setLyricsW(int Weight)
{
    Song::LyricsW = Weight;
}

/**
 * prints score
 */
void Song::scorePrint(int score)
{
    cout << title << PRINT_SEP << score << PRINT_SEP << Data << endl;
}


/**
 * sets a map of strings to be the Words obj.
 */
void Song::setWords(map<string, std::array<double, 2>> &newWords)
{
    Song::Words = newWords;
}
