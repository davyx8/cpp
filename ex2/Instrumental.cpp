/**
* author: davyx8
* date:  29.12.15
*/
#include <vector>
#include <cmath>
#include "Instrumental.h"

extern map<string, std::array<double, 2>> Song::Words;

/**
* constructor default
*/
SongInstrumental::SongInstrumental() : Song()
{
}

/**
* constructor simple
*/
SongInstrumental::SongInstrumental(string name, map<string, int> &tags, string performer,
                                   map<string, int> &instruments, int bpm) :
                                                        Song(name, tags,
                                                             "performed by: " + performer),
                                                        _bpm(bpm),
                                                        _performer(performer),
                                                        _instruments(instruments)
{
}

/**
* constructor simple
*/
int SongInstrumental::_bpmScoreCalculator(const string &query)
{


    if(Song::Words.find(query) == Song::Words.end() || _bpm == 0)
    {
        return 0;
    }

    std::array<double , 2> knownWordData = Song::Words[query];

    int result;
    double difference = _bpm - knownWordData[0];
    double sqDiff = pow(difference , 2);
    double variance = pow(knownWordData[1] , 2);
    double exponent = -(sqDiff / (2*variance));
    result = (int)floor(exp( exponent)*Song::bpmW );

    return result;
}

/**
* returns score 
*/
int SongInstrumental::getScore(const string &query)
{

//    cout<<"Words:"<<endl;
//    for(map<const string,vector<double>>::iterator it = Song::Words.begin(); it != Song::Words.end(); ++it) {
//
//        cout << it->first << "\n";
////        for(vector<double>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
//        cout << it->second[0] << "\n";
//            cout << it->second[1] << "\n";
////        }
//    }
//    cout<<"instruments queries:"<<endl;
//    for(map<string,int>::iterator it = _instruments.begin(); it != _instruments.end(); ++it) {
//        cout << it->first << "\n";
//        cout << it->second << "\n";
//    }
    int tagScore = tags[query] * Song::tagW;
    int instrumentScore = _instruments[query] * Song::instrumentW;
int bpmScore = _bpmScoreCalculator(query);


return bpmScore + instrumentScore + tagScore;
}