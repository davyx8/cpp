/**
* author: davyx8
* date:  29.12.15
*/

#ifndef SONG_H
#define SONG_H


#include <iostream>
#include <map>
#include <vector>

using namespace std;

/**
* a generic class for songs
* */
class Song
{
protected:
string title;
map<string, int> tags;
string Data;
static int tagW;
static int instrumentW;
static int bpmW;
static int LyricsW;
static map<string, std::array<double, 2>> Words;
public:

/**
* default constructor
*/
Song();
/**
* default simple constructor
*/
Song(string& title, map<string, int>& tags, string printedData);
/**
* default destructor
*/
virtual ~Song() = 0;
/**
 * prints score
 */
void scorePrint(int score);
/**
 * prints score
 */
virtual int getScore(const string& query) = 0;
/**
 * set tag weight
 */
static void setTagW(int Weight);

/**
 * sets instrument Weight
 */
static void setInstrumentsW(int newWeight);
/**
 * sets bpm Weight
 */
static void setBpmW(int newWeight);

/**
 * sets lyrics Weight
 */
static void setLyricsW(int newWeight);

/**
 * sets a map of strings to be the Words obj.
 */
static void setWords(map<string, std::array<double, 2>>& Words);

};


#endif
