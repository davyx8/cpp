cmake_minimum_required(VERSION 3.3)
project(ex2)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES
    MIR.cpp
    ParserFuncs.cpp
    ParserFuncs.h
    Song.cpp
    Song.h
        Instrumental.cpp
        Instrumental.h
        Lyrical.cpp
        Lyrical.h
    SortHelper.cpp
    SortHelper.h)

add_executable(ex2 ${SOURCE_FILES})