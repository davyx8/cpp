/**
* author: davyx8
* date:  29.12.15
*/
#ifndef SONGINSTRUMENTAL_H
#define SONGINSTRUMENTAL_H


#include "Song.h"

/**
* a class representing a wordless song.
*/
class SongInstrumental : public Song
{
private:
int _bpm;
string _performer;
map<string, int> _instruments;
/**
 * calculates the likelihood of a song to match according to bpm proximity to
 * known words avg bpm.
*/
int _bpmScoreCalculator(const string &query);

public:
/**
 *default constructor
 */
SongInstrumental();

/**
  *simple constructor
  */
SongInstrumental(string name, map<string, int>& tags, string performer,
                 map<string, int>& instruments, int bpm = 0);
/**
  *calculate score
  *
  */
int getScore(const string& query);
};


#endif