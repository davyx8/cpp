/**
* author: davyx8
* date:  29.12.15
*/
#ifndef LYRICAL_H
#define LYRICAL_H


#include "Song.h"

/**
* Generic song class with lyrics
*/
class Lyrical : public Song
{
private:
map<string, int> lyrics;
string author;
public:
/**
 * default constructor
 */
Lyrical();

/**
 *simple constructor
 */
Lyrical(string name, map<string, int> &tags, string author, map<string, int> &lyrics);

/**
 *
 * return the songs score
 */
int getScore(const string& query);
};


#endif
