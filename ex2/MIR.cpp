/**
 *
* author: davyx8
* date:  29.12.15
 *The main program to run queries
 */
#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <sstream>
#include <string.h>
#include "Song.h"
#include "SortHelper.h"
#include "Lyrical.h"
#include "Instrumental.h"
using namespace std;

#define FUCKEDFILE "Error! Can't open file: "
#define SEMICOLON ":"
#define EMPTY ""

/**
 *sets Parameters
 */
void setParams(const string& paramFilePath);


/**
 *
 * parses files and populates vector
 */
void songsVectorPopulate(const string& filePath, vector<Song*>& songs);

/**
 *
 * read file and q find the songs in the vec
 *
 */
void findSongs(string& path, const vector<Song*>& songsVec);

/**
 *
 * based on stream creates a Song object
 *
 */
Song* createSong(ifstream &inStream, string& line);

/**
 *
 * based on parameters given in input, creates a Song Obj.
 *
 */
Song* songCreator(const string &title, map<string, int> &tags, const string &identifier,
                  map<string, int> &additionalData, string &printedData, ifstream &inStream);

/**
 * deletes all song pointers in a vector.
 */
void vectorDestroyer(vector<Song *>& toClear);


/**
 * iterates stream till we get a real line
 */
int iterateEmpties(ifstream&inStream, string& line);

/**
 * separates int from semicolon
 */
int parseInt(const string& line);

/**
 * parses line to tags
 */
void tagParser(const string &line, map<string, int>& toUpdate);

/**
 *populates a word Vector
 */
void wordPopulator(const string& line, map<string, int>& toUpdate);


/**
*
 * parses line to words
 */
string wordParser(const string& line, int& first, int& second);

/**
*
 * the main function
 * runs evs.
 * receivs 3 file paths
 */
int main(int argc, char* argv[])
{
    if(argc != 4)
    {
        cout << "MIR <songs_data_file> <learned_parameters_file> <queries_file>" << endl;
        return 0;
    }

    string path = argv[2];

    setParams(path);
    path = argv[1];
    vector<Song*> songsVec = vector<Song*>();
    songsVectorPopulate(path, songsVec);

    path = argv[3];
    findSongs(path, songsVec);
    vectorDestroyer(songsVec);
    return 0;
}

void vectorDestroyer(vector<Song *>& toClear)
{
    int i = 0;
    while( i < (int)toClear.size())
    {
        delete toClear[i];
        i++;
    }
}

void findSongs(string& path, const vector<Song*>& songsVec)
{
    ifstream inStream(path.c_str());
    if(!inStream.is_open())
    {
        cerr << FUCKEDFILE << path << endl;
        return;
    }
    string query = EMPTY;
    while(iterateEmpties(inStream, query))
    {
        cout << "----------------------------------------" << endl;
        cout << "Query word: " << query << endl << endl;
        int  vecSize = songsVec.size();
        SortHelper helper = SortHelper();
        int score;
        int scores[vecSize];
        int i = 0;
        while( i < vecSize )
        {
            score = songsVec[i]->getScore(query);
            scores[i] = score;
            helper.addItem(scores[i]);
             i++;
        }
//        cout<<helper<<endl;
        vector<int> newIndexes = helper.getSortedOrderInVector();
        int newIdx;
        for( int j = 0; j < vecSize ; j++)
        {
            newIdx = newIndexes[j];
            score = scores[newIdx];
            if (score <= 0)
            {
                break;
            }
            songsVec[newIdx]->scorePrint(score);
        }
    }
}



void songsVectorPopulate(const string& path, vector<Song*>& songs)
{
    ifstream inStream(path.c_str());
    if (!inStream.is_open())
    {
        cerr << FUCKEDFILE << path << endl;
    }
    else
    {
        string line = EMPTY;
        while(iterateEmpties(inStream, line) && line != "***")
        {
            Song* newSong = createSong(inStream, line);
            songs.push_back(newSong);
        }
    }
}

Song* createSong(ifstream &inStream, string& line)
{
    Song* result;
    string title, identifier, printedData;
    map<string, int> tags = map<string, int>(), additionalData = map<string, int>();
    if(line == "=")
    {
        iterateEmpties(inStream, line);
    }
    title = line.substr(line.find(SEMICOLON) + 2, line.npos);
    iterateEmpties(inStream, line);
    int first  = line.find("{");
    int second = line.find("}");
    line = line.substr(first + 1, second - first - 1);
    tagParser(line, tags);
    iterateEmpties(inStream, line);
    identifier = line.substr(0, line.find(SEMICOLON));
    first  = line.find("{");
    second = line.find("}");
    line = line.substr(first + 1, second - first - 1);
    wordPopulator(line, additionalData);
    iterateEmpties(inStream, line);
    printedData = line.substr(line.find(SEMICOLON) + 2);

    result = songCreator(title, tags, identifier, additionalData, printedData, inStream);
    return result;
}

Song* songCreator(const string &title, map<string, int> &tags, const string &identifier,
                  map<string, int> &additionalData, string &printedData, ifstream &inStream)
{
    Song* newSong;
    if(identifier == "lyrics")
    {
        newSong = new Lyrical(title, tags, printedData, additionalData);
    }
    else
    {
        int bpm = 0;
        string line = EMPTY;
        iterateEmpties(inStream, line);
        if (line != "***" && line != "=")
        {
            line = line.substr(line.find(SEMICOLON) + 2);
            bpm = std::stoi(line);
        }
        newSong = new SongInstrumental(title, tags, printedData, additionalData, bpm);
    }
    return newSong;
}

void addKnownWord(const string& line, map<string, std::array<double, 2>>& knownWords)
{
    int first = line.find(SEMICOLON);
    int second = first;
    string knownWord = line.substr(0, first);
    double value;
    std::array<double, 2> newVector;
    int i = 0;
    while(i < 2)
    {
        first = second + 2;
        second = line.find(" ", first);

        value = std::stod(line.substr(first, second));
        newVector[i] = value;
        i++;
    }

    knownWords[knownWord] = newVector;
}
void setParams(const string& paramFilePath)
{
    ifstream inStream(paramFilePath.c_str());
    if(!inStream.is_open())
    {
        cerr << FUCKEDFILE << paramFilePath << endl;
        return;
    }

    string line = EMPTY;
    iterateEmpties(inStream, line);

    Song::setTagW(parseInt(line));

    iterateEmpties(inStream, line);

    Song::setLyricsW(parseInt(line));
    iterateEmpties(inStream, line);

    Song::setInstrumentsW(parseInt(line));
    iterateEmpties(inStream, line);

    Song::setBpmW(parseInt(line));

    map<string, std::array<double, 2>> knownWords = map<string , std::array<double, 2>>();
    while(iterateEmpties(inStream, line))
    {
        addKnownWord(line, knownWords);
    }
    Song::setWords(knownWords);
    inStream.close();

}


void tagParser(const string &line, map<string, int>& toUpdate)
{
    string tag;
    int tagWeight;
    int first = 0, second = 0;
    do
    {
        tag = wordParser(line, first, second);
        tagWeight = std::stoi(wordParser(line, first, second));
        toUpdate[tag] = tagWeight;
    } while(second != (int)line.npos);
}

void wordPopulator(const string& line, map<string, int>& toUpdate)
{
    int first = 0;
    int second = 0;
    string word = EMPTY;
    do
    {
        word = wordParser(line, first, second);
        toUpdate[word]++;
    } while (second!= (int) line.npos);
};

/**
*
 * parses line to words
 */
string wordParser(const string& line, int& first, int& second)
{
    first = line.find_first_not_of(" ", second);
    second = line.find(" ", first);
    return line.substr(first, second - first);
}

/**
 * iterates stream till we get a real line
 */
int iterateEmpties(ifstream&inStream, string& line)
{
    line = EMPTY;
    do
    {
        if(getline(inStream, line) == nullptr)
        {
            return 0;
        }
    }while (line == EMPTY);
    return 1;
}


/**
 * separates int from semicolon
 */
int parseInt(const string& line)
{
    int result = -1;
    int colon = line.find(SEMICOLON , 0);
    string x = line.substr(colon + 1 , line.length());
    result = std::stoi(x) ;
    return result;
}
