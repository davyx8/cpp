/**
 * MySet.h
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a Set, a data structure of distinct objects 
 *
 *  Methods: 
 *   MySet() - Constructor
 *  ~MySet()         - Destructor
 *  
 *  add                  - Add a string to the MySet. and add the element to
 *                         the begin of of the data structure.
 *                         If the element already exists , change its data to the
 *                         input parameter. 
 * 								parameters: string(key),double(data)
 *              	         return type void
 *
 *  remove               - Remove a string from the MySet. 
 *                         Return the numbr of removed elements (0/1)
 * 								parameters: key
 *              	         return type size_t 
 *
 *  isInSet            - Return true if the element is in the Set, or false otherwise.
 *                         If the element exists in the Set, return in 'data' its appropriate data
 *                         value. Otherwise don't change the value of 'data'.
 * 								parameters: key,data
 *									return type: bool
 *
 *  size                 - Return number of elements stored in the Set.
 *              	         return type size_t 
 *
 *  sumSet              - Return sum of all data elements stored in the Set.
 *              	         return type double
 *
 *
 *  printSet             - print Set contents.
 *
 *
 *  totWeight            - Return the total myHashFunction weight of all the Set keys
 *              	         return type double
 *
 * --------------------------------------------------------------------------------------
 
#ifndef MY_SET_H
#define MY_SET_H




 */

#ifndef MY_SET_H
#define MY_SET_H
#include <iostream>



/**
 * linked list class.
 * two way list that hold nodes contain string key and double data
 */
class MySet
{
private:
	/**
	 * simple node class for MySet.
	 * contain key, data and pointers to next and prev nodes.
	 */
class MySetNode;
	/**
	 * delete a node, connect the list on both sides
	 */
	void _deleteNode(MySetNode* currentNode);

public:


	/**
	 * pointers to first and last nodes
	 */
	MySetNode *_first, *_last;
	/**
	 * simple constructor
	 */
	MySet();
	int _size;

	/**
	 * The hash function.
	 * Input parameter - any C++ string.
	 * Return value: the hash value - the sum of all the characters of the string
	 *   (according to their ASCII value). The hash value of the
	 *   empty string is 0 (since there are no characters, their sum according to
	 *   the ASCII value is 0).
	 * NOTE: In a better design the function would have belong to the string class
	 *	 (or to a class that is derived from std::string). We implement it as a "stand 
	 *	 alone" function since you didn't learn inheritance yet. Keep the function
	 *	 global (it's important to the automatic tests).*/
	static int myHashFunction(const std::string &str);
	/**
	 * simple constructor
	 */
	int size() const;
	/**
	 * copy constructor
	 */
	MySet(const MySet& other);
	
	/**
	 * simple destructor
	 */
	virtual ~MySet();
	/**
	 * adds key to set
	 */
	void add( std::string key, double value);
	/**
	 * removes key from set
	 */
	int remove (std::string deleteKey);
	/**
	 * checks if key  is In Set
	 */
	bool isInSet(std::string key, double& ref) const;
	/**
	 * totWeight - total weight
	 */
	int totWeight() const;
	/**
	 * print Set
	 */
	void printSet () const;
	/**
	 * sums the Set
	 */
	double sumSet() const;

	/**
	 * hasama 
	 */
	MySet& operator=(const MySet&);
	/**
	 *minus
	 */
	MySet operator-(const MySet&);
	/**
	 * or
	 */
	MySet operator|(const MySet&);
	/**
	 * and
	 */
	MySet operator&(const MySet&);
	/**
	 * bigger
	 */
	bool operator>( MySet&);
	/**
	 * smaller
	 */
	bool operator<( MySet&);
	/**
	 * equals
	 */
	bool operator==( const MySet&);

};

#endif /* MYSet_H_ */