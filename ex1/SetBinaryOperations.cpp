/**
 * This file is demonstrating the operators overloading
 * That was done in this exrecise
 */

#include <iostream>
#include <string>
#include "MySet.h"

#define NUM_OF_ARRAYS 4

int main()
{   
    //An array of sets
    MySet setsARR[NUM_OF_ARRAYS];
    //Boolean variables to keep track of comparing results.
    bool isEqual,isLarger,isSmaller;
    //Sets for domonstrating the binary operations
    MySet subSet,unionSet,interSet;
    //String to be the keys of the sets
    std::string set1Key1 = "a";  
    std::string set1Key2 = "b";
    std::string set2Key1 = "b";  
    std::string set2Key2 = "c";
    std::string set3Key1 = "c";  
    std::string set3Key2 = "d";
    std::string set4Key1 = "d";  
    std::string set4Key2 = "e";
    //Adding elements to each set
    setsARR[0].add(set1Key1,1.2);
    setsARR[0].add(set1Key2,-2.5);
    setsARR[1].add(set2Key1,4.5);
    setsARR[1].add(set2Key2,-0.0);
    setsARR[2].add(set3Key1,5.2);
    setsARR[2].add(set3Key2,1.1);
    setsARR[3].add(set4Key1,4.5);
    setsARR[3].add(set4Key2,-12.1);
    
    //Demonstrating the equlity operator
    MySet equalSet=setsARR[0];
    std::cout<<"Printing set 0\n";
    setsARR[0].printSet();
    std::cout<<"Printing set 0 copy\n";
    equalSet.printSet();
    
    //Demonstrating the smaller comparison operators
    //-----------------------------------------------
    
    //Demonstrating the larger then operator overloading
    isSmaller = setsARR[0] < setsARR[1];
    if(isSmaller)
    {
        std::cout<<"Set 0 is smaller then set 1"<<std::endl;
    }
    //As we defined it set[1] is smaller then set[2]
    isSmaller = setsARR[2] < setsARR[0];
    if(!isSmaller)
    {
        std::cout<<"Set 2 is larger then set 0"<<std::endl;
    }
    
    //Demonstrating the larger then operator overloading
    isLarger = setsARR[1] > setsARR[0];
    if(isLarger)
    {
        std::cout<<"Set 1 is larger then set 0"<<std::endl;
    }
    isLarger = setsARR[0] > setsARR[2];
    if(!isLarger)
    {
        std::cout<<"Set 0 is smaller then set 2"<<std::endl;
    }
    //Demonstrating the eqaul to operator overloading
    isEqual = setsARR[0] == equalSet;
    if(isEqual)
    {
        std::cout<<"Set 0 equals set 4"<<std::endl;
    }
    isEqual = setsARR[3] == setsARR[2];
    if(!isEqual)
    {
        std::cout<<"Set 3 does equal set 2"<<std::endl;
    }
    
    //Demostrating the binary operators overloading
    //----------------------------------------------
    
    //Demonstrating the sutraction operator
    subSet = setsARR[0]-setsARR[1];
    std::cout<<"Printing set 0\n";
    setsARR[0].printSet();
    std::cout<<"Printing set 1\n";
    setsARR[1].printSet();
    std::cout<<"Printing (set 0 - set 1)\n";
    subSet.printSet();
    
    subSet = setsARR[0]-setsARR[2];
    std::cout<<"Printing set 0\n";
    setsARR[0].printSet();
    std::cout<<"Printing set 2\n";
    setsARR[2].printSet();
    std::cout<<"Printing (set 0 - set 2)\n";
    subSet.printSet();
    
    //Demonstrating the '&' (intersection) operator
    interSet = setsARR[1]&setsARR[2];
    std::cout<<"Printing set 1\n";
    setsARR[1].printSet();
    std::cout<<"Printing set 2\n";
    setsARR[2].printSet();
    std::cout<<"Printing (set 1 & set 2)\n";
    interSet.printSet();
    
    interSet = setsARR[1]&setsARR[3];
    std::cout<<"Printing set 1\n";
    setsARR[1].printSet();
    std::cout<<"Printing set 3\n";
    setsARR[3].printSet();
    std::cout<<"Printing (set 1 & set 3)\n";
    interSet.printSet();
    
    //Demonstrating the '|' (union) operator
    unionSet = setsARR[2]|setsARR[3];
    std::cout<<"Printing set 2\n";
    setsARR[2].printSet();
    std::cout<<"Printing set 3\n";
    setsARR[3].printSet();
    std::cout<<"Printing (set 2 | set 3)\n";
    unionSet.printSet();
    
    unionSet = setsARR[1]|setsARR[3];
    std::cout<<"Printing set 1\n";
    setsARR[1].printSet();
    std::cout<<"Printing set 3\n";
    setsARR[3].printSet();
    std::cout<<"Printing (set 1 | set 3)\n";
    unionSet.printSet();
    
    return 0;
}