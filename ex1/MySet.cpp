/*
* MySet.cpp
*
*  Created on: Dec 14, 2015
*      Author: davyx8
moto: The dude abides man!
*/
#include "MySet.h"
#include <iostream>
/**
* simple node for ll set
*/
class MySet::MySetNode
{
    public:
    double _data;
    std::string _key;
    MySetNode *_next, *_prev;
    MySetNode(std::string key, double data)
    {
        _key = key;
        _data = data;
        _next = NULL;
        _prev = NULL;
    }
};
int MySet::myHashFunction(const std::string &str)
{
    int sum = 0;
    int i = 0;
    while(str[i] != '\0')
    {
        sum += (int)str[i];
        i ++;
    }
    return sum ;
}
/**
* muddafucking constructor
*/
MySet::MySet()
{
    _first = NULL;
    _last = NULL;
    _size = 0;
}
/**
* muddafucking copy constructor
*/
MySet::MySet(const MySet& other)
{
    this->_last = this->_first = NULL;
    for(MySetNode* p = other._first ; p != NULL ; p = p->_next)
    {
        this->add(p->_key , p->_data);
    }
}
/**
* add a new muddafucking node to list
*/
void MySet:: add(std::string key, double value)
{
    if (_first == NULL)
    {
        _first = new MySetNode(key, value);
        _last = _first;
        _size = 1;
    }
    else
    {
        double w = 0.0;
        double& refe = w;
        bool inList = this->isInSet(key , refe);
        if(!inList)
        {
            MySetNode *newNode = new MySetNode(key, value);
            _first->_prev = newNode;
            newNode->_next = _first;
            _first = newNode;
            _size++;
        }
        else
        {
            MySetNode * currentNode = _first;
            while (!(currentNode == NULL))
            {
                if (currentNode->_key.compare(key) == 0)
                {
                    currentNode->_data = value;
                    return;
                }
                currentNode = currentNode->_next;
            }
        }
    }
}
/**
* delete a muddafucking node
*/
void MySet::_deleteNode(MySetNode* currentNode)
{
    if (!(currentNode->_next == NULL && currentNode->_prev == NULL))
    {
        if (currentNode->_next != NULL)
        {
            currentNode->_next->_prev = currentNode->_prev;
        }
        if (currentNode->_prev != NULL)
        {
            currentNode->_prev->_next = currentNode->_next;
        }
    }
    else
    {
        _first = NULL;
        _last = NULL;
    }
    delete(currentNode);
}
/**
* remove all muddafucking nodes with current key as their key
*/
int MySet::remove (std::string deleteKey)
{
    int numOfDeleted = 0;
    if (_first == NULL)
    {
        return 0;
    }
    MySetNode* currentNode = _first;
    while(currentNode->_next != NULL)
    {
        if (currentNode->_next->_key.compare(deleteKey) == 0)
        {
            _deleteNode(currentNode->_next);
            _size--;
            numOfDeleted++;
        }
        else
        {
            if(currentNode->_next != NULL)
            {
                currentNode = currentNode->_next;
            }
        }
    }
    _last = currentNode;
    if (_first->_key.compare(deleteKey) == 0)
    {
        numOfDeleted++;
        if (_first != _last)
        {
            _first = _first->_next;
        }
        _size--;
        _deleteNode(currentNode);
    }
    return numOfDeleted;
}
/**
* this fucking method check if a muddafucking node containing a given key is in list,
* if a muddafucking node exist, the method return muddafucking  true and change the given reference the muddafucking node's value
*/
bool MySet::isInSet(std::string key, double& ref)const
{
    MySetNode * currentNode = _first;
    while (!(currentNode == NULL))
    {
        if (currentNode->_key.compare(key) == 0)
        {
            ref = currentNode->_data;
            return true;
        }
        currentNode = currentNode->_next;
    }
    return false;
}
/**
* method print list of all  muddafucking nodes.
*/
void MySet::printSet() const
{
    if (_first == NULL)
    {
        std::cout << "Empty";
    }
    else
    {
        MySetNode* currentNode = _first;
        while (currentNode!=NULL)
        {
            std::cout << currentNode->_key << "," << currentNode->_data << "\n";
            currentNode = currentNode->_next;
        }
    }
}
int MySet::totWeight() const
{
    int sum = 0;
    if (_first == NULL)
    {
        return sum;
    }
    else
    {
        MySetNode* currentNode = _first;
        while (currentNode != NULL)
        {
            //  std::cout << currentNode->_key << "," << currentNode->_data << "n";
            sum += this->myHashFunction(currentNode->_key);
            currentNode = currentNode->_next;
        }
    }
    return sum;
}
/**
* this method calculate the sum of  muddafucking nodes in list.
*/
double MySet::sumSet()const
{
    double sum = 0;
    MySetNode* cusrrentNode = _first;
    while (cusrrentNode != NULL)
    {
        sum += cusrrentNode->_data;
        cusrrentNode = cusrrentNode->_next;
    }
    return sum;
}
/**
* class muddafucking destructor
*/
MySet::~MySet()
{
    if (_first == NULL)
    {
        _last = NULL;
    }
    else
    {
        while (_first->_next != NULL)
        {
            _deleteNode(_first->_next);
        }
        _deleteNode(_first);
    }
}
MySet& MySet::operator=(const MySet& other)
{
    if (this == &other)
    {
        return *this;
    }
    this->~MySet();
    for(MySetNode *p = other._first ; p != NULL ; p = p->_next)
    {
        this->add(p->_key , p->_data);
    }
    return *this;
}
MySet MySet::operator-(const MySet& other)
{
    double w = 0.0;
    double& refe = w;
    MySet newSet =  MySet() ;
    for(MySetNode *p = this->_first ; p != NULL ; p = p->_next)
    {
        if(!other.isInSet(p->_key , refe))
        {
            newSet.add(p->_key , p->_data) ;
        }
    }
    return newSet ;
}
MySet MySet::operator&(const MySet& other)
{
    double w = 0.0;
    double& refe = w;
    MySet newSet =  MySet() ;
    for(MySetNode *p = this->_first ; p != NULL ; p = p->_next)
    {
        if(other.isInSet(p->_key , refe))
        {
            newSet.add(p->_key , p->_data) ;
        }
    }
    return newSet ;
}
MySet MySet::operator|(const MySet& other)
{
    if (this == &other)
    {
        return *this;
    }
    MySet newSet =  MySet() ;

    for(MySetNode *p = other._first ; p != NULL ; p = p->_next)
    {
        newSet.add(p->_key , p->_data) ;
    }
    for(MySetNode *p = this->_first ; p != NULL ; p = p->_next)
    {
        newSet.add(p->_key , p->_data) ;
    }
    return newSet ;
}
bool MySet::operator>(  MySet& other)
{
    int thisTot = this->totWeight();
    int otherTot = other.totWeight();
    return thisTot > otherTot;
}
bool MySet::operator<( MySet& other)
{
    int thisTot = this->totWeight();
    int otherTot = other.totWeight();
    return thisTot < otherTot;
}
bool MySet::operator == (const MySet& other)
{
    int thisTot = this->totWeight();
    int otherTot = other.totWeight();
    return thisTot == otherTot;
}
/**
* advances the iterator
*/

/**
* return true iff current node is not empty
*/

int MySet::size() const
{
    return _size;
}

/**
* return current key
*/
